import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserTableComponent } from './components/employe-table/employe-table.component';
import { HeaderComponent } from './components/header/header.component';
import { UserModifyComponent } from './components/user-modify/user-modify.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { LoginComponent } from './components/login/login.component';
import { MaterialTableComponent } from './components/material-table/material-table.component';
import { CreateMaterialComponent } from './components/create-material/create-material.component';
import { ModifyMaterialComponent } from './components/modify-material/modify-material.component';

@NgModule({
  declarations: [
    AppComponent,
    UserTableComponent,
    HeaderComponent,
    UserModifyComponent,
    CreateUserComponent,
    LoginComponent,
    MaterialTableComponent,
    CreateMaterialComponent,
    ModifyMaterialComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
