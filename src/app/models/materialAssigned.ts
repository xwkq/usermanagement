export class MaterialAssigned{
    id!: number;
    name!: string;
    image!:string;
    state!: string;
    reference!: string;
}