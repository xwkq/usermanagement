import { MaterialAssigned } from "./materialAssigned";

export class Employe{
    age!: string;
    firstname!: string;
    email!: string;
    password!: string;
    lastname!: string;
    id!: number;
    materialAssigned!: MaterialAssigned;
    num!: string;
    profilImage!: string;
    validate!: boolean;
}