import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employe } from 'src/app/models/employe';
import { MaterialAssigned } from 'src/app/models/materialAssigned';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { EmployeService } from 'src/app/services/employe.service';
import { MaterialService } from 'src/app/services/material.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {


  userForm!: FormGroup;

  employes!: Employe[];
  materialAssigned!: MaterialAssigned[];

  nameOfMaterialAssign: string = '';

  materials: MaterialAssigned = {
    id: 0,
    reference: '',
    name: '',
    state: '',
    image: '',
  };

  employe: Employe = new Employe;

  isConnected: boolean = false;

  constructor(private formBuilder: FormBuilder, private auth: AuthServiceService, private employeService: EmployeService, private router: Router, private materialService: MaterialService) { }

  ngOnInit(): void {
    this.auth.isConnect.subscribe((isConnected) =>{
      this.isConnected = isConnected;
    });
    this.materialService.getMaterials().subscribe(m => this.materialAssigned = m);

    this.userForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      age: ['', Validators.required],
      password: ['', Validators.required,],
      num: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      profilImage: [''],
      nameOfMaterialAssign: ['']
    });
  }

  create(){
    if(this.userForm.valid){
      this.employe.firstname = this.userForm.value.firstname;
      this.employe.lastname = this.userForm.value.lastname;
      this.employe.age = this.userForm.value.age;
      this.employe.password = this.userForm.value.password;
      this.employe.email = this.userForm.value.email;
      this.employe.num = this.userForm.value.num;
      this.employe.profilImage = this.userForm.value.profilImage;

      const m = this.materialAssigned.map(materiel => materiel).filter(m => m.name === this.userForm.value.nameOfMaterialAssign);

      if(m.length <= 0){
        this.employe.materialAssigned = this.materials;
      }else{
        for(let material of m){
          this.employe.materialAssigned = material;
        }
      }
      
      this.employeService.create(this.employe);
      this.router.navigate(['/users']);
    }
  }
}
