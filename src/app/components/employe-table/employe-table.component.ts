import { filter } from 'rxjs';
import { EmployeService } from './../../services/employe.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employe } from 'src/app/models/employe';
import { MaterialAssigned } from 'src/app/models/materialAssigned';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { MaterialService } from 'src/app/services/material.service';

@Component({
  selector: 'app-employe-table',
  templateUrl: './employe-table.component.html',
  styleUrls: ['./employe-table.component.css']
})
export class UserTableComponent implements OnInit {

  constructor(private employeService: EmployeService, private authService: AuthServiceService, private router: Router) { }

  isConnected: boolean = false;
  employes!: Employe[];

  ngOnInit(): void {
    this.authService.isConnect.subscribe((isConnected) => {
      this.isConnected = isConnected;
    });
    
    this.employeService.getEmployees().subscribe(e => this.employes = e);
  }

  updateUser(employe: Employe){
    this.router.navigate(['userUpdate', employe.id]);    
  }

  deleteUser(employe: Employe){
    this.employeService.delete(employe.id);
    this.employeService.getEmployees().subscribe(e => this.employes = e);
  }

}
