import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { filter } from 'rxjs';
import { Employe } from 'src/app/models/employe';
import { MaterialAssigned } from 'src/app/models/materialAssigned';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { EmployeService } from 'src/app/services/employe.service';
import { MaterialService } from 'src/app/services/material.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-create-material',
  templateUrl: './create-material.component.html',
  styleUrls: ['./create-material.component.css']
})
export class CreateMaterialComponent implements OnInit {

  materialForm!: FormGroup

  material: MaterialAssigned = {
    id: 0,
    reference: '',
    name: '',
    state: '',
    image: '',
  };
  isConnected: boolean = false;

  constructor(private formBuilder: FormBuilder, private auth: AuthServiceService, private materialService: MaterialService, private router: Router) { }

  ngOnInit(): void {
    this.auth.isConnect.subscribe((isConnected) =>{
      this.isConnected = isConnected;
    });

    this.materialForm = this.formBuilder.group({
      name: ['', Validators.required],
      state: ['', Validators.required],
      reference: ['', Validators.required],
      image: [''],
    });

  }
  
  create(){

    if(this.materialForm.valid){

      this.material.image = this.materialForm.value.image;
      this.material.state = this.materialForm.value.state;
      this.material.name = this.materialForm.value.name;
      this.material.reference = this.materialForm.value.reference;
      
      this.materialService.createMaterial(this.material);
      this.router.navigate(['/materials']);
    }
  }

}
