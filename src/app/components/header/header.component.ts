import { filter } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { EmployeService } from 'src/app/services/employe.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isConnected: boolean = false;
  searchName: string = '';

  constructor(private authService: AuthServiceService, private router: Router, private employeService: EmployeService) { 
  }

  ngOnInit(): void {
    this.authService.isConnect.subscribe((isConnected) => {
      this.isConnected = isConnected;
    });
  }

  connect(){
    this.router.navigate(['login']);
  }

  disconnected(){
    this.authService.disconnect();

  }

  search(){
    this.employeService.employeesFilteredByName(this.searchName);
  }

}
