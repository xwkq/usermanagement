import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterialAssigned } from 'src/app/models/materialAssigned';
import { MaterialService } from 'src/app/services/material.service';

@Component({
  selector: 'app-modify-material',
  templateUrl: './modify-material.component.html',
  styleUrls: ['./modify-material.component.css']
})
export class ModifyMaterialComponent implements OnInit {

  material!: MaterialAssigned;

  id!: number;

  constructor(private route: ActivatedRoute, private materialService: MaterialService, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.materialService.getMaterialById(this.id).subscribe((m) => this.material = m);    
    });

    
  }

  save(){
    this.materialService.updateMaterial(this.material);
    this.router.navigate(['/materials']);
  }

}
