import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employe } from 'src/app/models/employe';
import { MaterialAssigned } from 'src/app/models/materialAssigned';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { EmployeService } from 'src/app/services/employe.service';
import { MaterialService } from 'src/app/services/material.service';

@Component({
  selector: 'app-material-table',
  templateUrl: './material-table.component.html',
  styleUrls: ['./material-table.component.css']
})
export class MaterialTableComponent implements OnInit {

  
  isConnected: boolean = false;
  materials: MaterialAssigned[] = [];

  
  constructor(private authService: AuthServiceService, private materialService: MaterialService, private router: Router) { }

  ngOnInit(): void {
    this.authService.isConnect.subscribe((isConnected) => {
      this.isConnected = isConnected;
    });
    
    this.materialService.getMaterials().subscribe(m => this.materials = m);
  }

  updateMaterial(material: MaterialAssigned){
    this.router.navigate(['materialUpdate', material.id]);    
  }

  deleteMaterial(material: MaterialAssigned){
    this.materialService.deleteMaterial(material.id)
  }

}
