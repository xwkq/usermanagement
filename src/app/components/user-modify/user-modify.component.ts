import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeService } from 'src/app/services/employe.service';

@Component({
  selector: 'app-user-modify',
  templateUrl: './user-modify.component.html',
  styleUrls: ['./user-modify.component.css']
})
export class UserModifyComponent implements OnInit {

  employe!: any;
  
  firstname: string = '';
  email: string = '';
  lastname: string = '';
  age: string = '';
  password: string = '';
  num: string = '';
  profileImage: string = '';

  id!: number;


  constructor(private route: ActivatedRoute, private employeService: EmployeService, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
    this.id = params['id'];
    this.employeService.getEmployeById(this.id).subscribe((e) => this.employe = e);    
    });
  }

  save(){
    this.employeService.update(this.employe);
    this.router.navigate(['/users']);
  }
}
