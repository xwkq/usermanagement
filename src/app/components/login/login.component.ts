import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user!: any;
  
  constructor(private userService: UserService, private auth: AuthServiceService, private router: Router) { }

  ngOnInit(): void {
    this.userService.usersObservable.subscribe((u) => 
    this.user = u);
  }

  login(){
    const exist = this.userService.userExist(user => user.username === this.user.username && user.password === this.user.password);

    if(exist){
      this.auth.connect();
      this.router.navigate(['/users']);
    }else{
      console.log('user not found');
    }
  }

}
