import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  users: any = this.httpClient.get<User[]>('http://localhost:3000/users');

  private usersSubject = new BehaviorSubject<User[]>([]);
  usersObservable = this.usersSubject.asObservable();
  
  private initUser =  new BehaviorSubject(this.usersSubject.getValue());
  
  constructor(private httpClient: HttpClient) { 
    this.users.subscribe((data: User[]) => this.usersSubject.next(data));
  }

  userExist(criteria: (user: User) => boolean): boolean {
    const currentUser = this.usersSubject.value;
    return currentUser.some(criteria);
  }
}
