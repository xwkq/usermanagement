import { Injectable } from '@angular/core';
import { Employe } from '../models/employe';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, filter, map, switchMap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeService {
  
  private employesSubject = new BehaviorSubject<Employe[]>([]);
  employesObservable = this.employesSubject.asObservable();

  constructor(private httpClient: HttpClient) {
    this.save();
  }  

  create(employe: Employe){
    this.httpClient.post<Employe>("http://localhost:3000/employees", employe).subscribe(() => this.save());
  }
  update(employe: Employe){
    this.httpClient.put<Employe>("http://localhost:3000/employees/"+ employe.id, employe).subscribe(() => this.save());
  }
  delete(id: number) {
  this.httpClient.delete<Employe>("http://localhost:3000/employees/"+id).subscribe(() => this.save());
  }
  getEmployeById(id: number){   
    return this.httpClient.get<Employe[]>('http://localhost:3000/employees/'+id);
  }
  getEmployees(){
    return this.employesSubject.asObservable();
  }
  employeesFilteredByName(firstname: string){
    this.httpClient.get<Employe[]>('http://localhost:3000/employees').pipe(
      map(employe => employe.filter(e => e.firstname.includes(firstname)))
    ).subscribe(filteredEmploye => {
      this.employesSubject.next(filteredEmploye);
    });
  }

  save(){
    this.httpClient.get<Employe[]>('http://localhost:3000/employees').subscribe(e => {
      this.employesSubject.next(e)
    });
  }

}
