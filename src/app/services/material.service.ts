import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MaterialAssigned } from '../models/materialAssigned';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  private materialsSubject = new BehaviorSubject<MaterialAssigned[]>([]);
  materialObservable = this.materialsSubject.asObservable();

  constructor(private httpClient: HttpClient) {
    this.save();
   }


  getMaterials(){
    return this.materialsSubject.asObservable();
  }

  createMaterial(material: MaterialAssigned){
    this.httpClient.post<MaterialAssigned>("http://localhost:3000/materiel", material).subscribe(() => this.save());
  }

  updateMaterial(material: MaterialAssigned){
    this.httpClient.put<MaterialAssigned>("http://localhost:3000/materiel/"+material.id, material).subscribe(() => this.save());
  }
  
  deleteMaterial(id: number){
    this.httpClient.delete<MaterialAssigned>("http://localhost:3000/materiel/"+id).subscribe(() => this.save());
  }

  getMaterialById(id: number){   
    return this.httpClient.get<MaterialAssigned>('http://localhost:3000/materiel/'+id);
  }

  getMaterialByName(name: string){   
    return this.httpClient.get<MaterialAssigned[]>('http://localhost:3000/materiel/'+name);
  }

  save(){
    this.httpClient.get<MaterialAssigned[]>('http://localhost:3000/materiel').subscribe(e => {
      this.materialsSubject.next(e)
    });
  }
}
