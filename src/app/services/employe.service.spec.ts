import { Employe } from './../models/employe';
import { TestBed } from '@angular/core/testing';

import { EmployeService } from './employe.service';

describe('UserService', () => {
  let service: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmployeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
