import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  
  private isConnected = new BehaviorSubject<boolean>(false);
  isConnect = this.isConnected.asObservable();

  constructor() { }

  connect(){
    this.isConnected.next(true);
  }
  disconnect(){
    this.isConnected.next(false);
  }
}
