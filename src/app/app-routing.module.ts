import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserTableComponent } from './components/employe-table/employe-table.component';
import { UserModifyComponent } from './components/user-modify/user-modify.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { LoginComponent } from './components/login/login.component';
import { MaterialTableComponent } from './components/material-table/material-table.component';
import { CreateMaterialComponent } from './components/create-material/create-material.component';
import { ModifyMaterialComponent } from './components/modify-material/modify-material.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'users', component: UserTableComponent},
  { path: 'materials', component: MaterialTableComponent},
  { path: 'userUpdate/:id', component: UserModifyComponent},
  { path: 'materialUpdate/:id', component: ModifyMaterialComponent},
  { path: 'createUser', component: CreateUserComponent},
  { path: 'createMaterial', component: CreateMaterialComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
